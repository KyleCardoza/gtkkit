#import <ObjFW/ObjFW.h>
#import <gtk/gtk.h>
#import "GTKWidget.h"

/** 
 * \class UIContainer
 * \brief A class to represent a generic container widget.
 *
 * UIContainer is the parent class of all UIKit container widget classes.
 */
@interface GTKContainer : GTKWidget

@end
