#import <ObjFW/ObjFW.h>
#import <gtk/gtk.h>
#import "GTKContainer.h"

@interface GTKBin : GTKContainer

- (GTKWidget *)childWidget;

@end
