#import <ObjFW/ObjFW.h>
#import <gtk/gtk.h>
#import "GTKContainer.h"

@interface GTKContainer (Properties)

@property (assign) unsigned int borderWidth;

@end
